package com.example.listadecompras;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    EditText edtNombre,edtPrecio,edtUnidades;
    Button btnAgregar;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtNombre=(EditText)findViewById(R.id.edtNombre);
        edtPrecio=(EditText)findViewById(R.id.edtPrecio);
        edtUnidades=(EditText)findViewById(R.id.edtUnidades);
        btnAgregar=(Button) findViewById(R.id.btnAgregar);

        final dataBase mydatabase=new dataBase(getApplicationContext());

        btnAgregar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mydatabase.agregarProducto(edtNombre.getText().toString(),edtPrecio.getText().toString(),edtUnidades.getText().toString());
                Toast.makeText(getApplicationContext(),"SE AGREGO CORRECTAMENTE",Toast.LENGTH_LONG).show();
            }
        });
    }


    public void onClick(View view) {

    }
}
