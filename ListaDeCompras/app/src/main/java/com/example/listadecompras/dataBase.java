package com.example.listadecompras;

import android.content.Context;
import android.database.DatabaseErrorHandler;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import androidx.annotation.Nullable;

public class dataBase extends SQLiteOpenHelper {

    private static final String NOMBRE_BD="mydatabase.bd";
    private static final int VERSION_BD=1;
    private static final String TABLA_PRODUCTO="CREATE TABLE PRODUCTO(PRODUCTO TEXT PRIMARY KEY, PRECIO TEXT, UNIDADES TEXT)";

    public dataBase(@Nullable Context context) {
        super(context, NOMBRE_BD, null, VERSION_BD);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(TABLA_PRODUCTO);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS "+TABLA_PRODUCTO);
        db.execSQL(TABLA_PRODUCTO);
    }

    public void agregarProducto(String nombre, String precio, String unidades){
        SQLiteDatabase bd=getWritableDatabase();
        if(bd!=null){
            bd.execSQL("INSERT INTO PRODUCTOS VALUES('"+nombre+"','"+precio+"','"+unidades+"')");
            bd.close();
        }
    }
}
